<!-- Begin Add Customer Modal -->
<div tabindex="-1" class="modal fade" id="add-customer-modal" ref="add_customer_modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" @keydown.enter="createCustomer()">
        <div class="modal-content">
            <div class="modal-header pmd-modal-bordered">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h2 class="pmd-card-title-text">Add Customer</h2>
            </div>
            <div class="modal-body">
                <div class="form-group pmd-textfield pmd-textfield-floating-label">
                    <label for="first_name">First Name</label>
                    <input type="text" class="mat-input form-control" name="first_name" v-model="newCustomer.first_name">
                    <span v-if="formErrors['first_name']" class="error text-danger">
                        @{{ formErrors['first_name'][0] }}
                    </span>
                </div>
                <div class="form-group pmd-textfield pmd-textfield-floating-label">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="mat-input form-control" name="last_name" v-model="newCustomer.last_name">
                    <span v-if="formErrors['last_name']" class="error text-danger">
                        @{{ formErrors['last_name'][0] }}
                    </span>
                </div>
                <div class="form-group pmd-textfield pmd-textfield-floating-label">
                    <label for="first-name">Email Address</label>
                    <input type="text" class="mat-input form-control" name="email" v-model="newCustomer.email">
                </div><br>

                <!--Inline Radio button-->
                <label class="radio-inline pmd-radio pmd-radio-ripple-effect">
                    <input type="radio" name="gender" value="m" v-model="newCustomer.gender">
                    <span for="inlineRadio1">male</span>
                </label>
                <label class="radio-inline pmd-radio pmd-radio-ripple-effect">
                    <input type="radio" name="gender" value="f" v-model="newCustomer.gender">
                    <span for="inlineRadio2">female</span>
                </label><br><br>

                <label class="checkbox-inline pmd-checkbox pmd-checkbox-ripple-effect">
                    <input type="checkbox" value="">
                    <span class="pmd-checkbox"> Accept Terms and conditions</span> 
                </label><br><br>

                <div class="pmd-modal-action">
                    <button class="btn pmd-btn-raised btn-primary" type="button" @click="createCustomer()">Submit</button>
                    <button data-dismiss="modal" class="btn btn-default" type="button">Discard</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Customer Modal End data-dismiss="modal"  -->
